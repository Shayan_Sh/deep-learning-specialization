The Deep Learning Specialization is our foundational program that will help you understand the capabilities, challenges, and consequences of deep learning and prepare you to participate in the development of leading-edge AI technology. 

In this Specialization, you will build neural network architectures such as Convolutional Neural Networks, Recurrent Neural Networks, LSTMs, Transformers, and learn how to make them better with strategies such as Dropout, BatchNorm, Xavier/He initialization, and more. You will master these theoretical concepts and their industry applications using Python and TensorFlow. You will tackle real-world case studies such as autonomous driving, sign language reading, music generation, computer vision, speech recognition, and natural language processing. 

AI is transforming many industries. The Deep Learning Specialization provides a pathway for you to gain the knowledge and skills to apply machine learning to your work, level up your technical career, and take the definitive step in the world of AI. Along the way, you will get career advice from deep learning experts from industry and academia.

Applied Learning Project
By the end of the program, you'll be ready to

• Build and train deep neural networks, implement vectorized neural networks, identify key parameters in architecture, and apply deep learning to your applications

• Use the best practices to train and develop test sets and analyze bias/variance for building DL applications, use standard neural network techniques, apply optimization algorithms, and implement a neural network in TensorFlow

• Diagnose and use strategies for reducing errors in ML systems, understand complex ML settings, and apply end-to-end learning, transfer learning, and multi-task learning

• Build a CNN, apply it to visual detection and recognition tasks, use neural style transfer to generate art, and apply these algorithms to image, video, and other 2D/3D data

• Build and train RNNs, GRUs, and LSTMs, apply RNNs to character-level language modeling, work with NLP and Word Embeddings, and use HuggingFace tokenizers and transformers to perform NER and Question Answering